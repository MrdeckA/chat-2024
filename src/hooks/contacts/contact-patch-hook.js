import { BadRequest } from "@feathersjs/errors"

export const contactPatchBefore = async (context) => {

  if (context.params?.user) {
    context.params.query = {
      ...context.params.query,
       $or : [
         {user1Id : context.params.user._id},
         {user2Id : context.params.user._id}
       ] 
    }
  }

  if (!context.data?.action || !['ANSWER_TO_REQUEST', 'BLOCKED_CONTACT'].includes(context.data.action)) {
    throw new BadRequest("Your action is not valid")
  }

  context.action = context.data.action

  if (context.data.action === 'ANSWER_TO_REQUEST') {
    const contact = await context.app.service("contacts").find({ query : {
      _id : context.id,
      user2Id: context.params.user._id,
      status: 'PENDING',
      $limit: 1
    }})

    if (!contact || contact.total < 1) {
      throw new BadRequest("You can response to this request")
    }

    const {status} = context.data;


    context.data = {
      status,
      addedAt: Date.now()
    }
    
  }

  else if (context.data.action === 'BLOCKED_CONTACT') {
    const contact = await context.app.service("contacts").find({ query : {
      _id : context.id,
      status: 'VALIDATED',
      $limit: 1
    }})

    if (!contact || contact.total < 1) {
      throw new BadRequest("You can blocked this contact")
    }
    if (context.data.isBlocked === undefined) {
      throw new BadRequest("You have to provide blocked status")
    }

    const {isBlocked} = context.data;

    if (contact.data[0].user1Id.toString() === context.params.user._id.toString()) {
      context.data = {
        user1Blocked: isBlocked,
      }
    } 
    
    else {
      context.data = {
        user2Blocked: isBlocked,
      }
    }


    
    
  }
}

export const contactPatchAfter = async (context) => {
  if (context.action === 'ANSWER_TO_REQUEST' && context.result.status === 'DECLINED') {
    context.app.service('contacts').remove(context.result._id)
  }
}
