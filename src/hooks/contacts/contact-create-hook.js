export const contactCreateBefore = async (context) => {

    const { user2Id } = context.data
    context.data = {
      user2Id,
      user1Id : context.params.user._id,
      addedAt : Date.now()
    }
}

export const contactCreateAfter = async (context) => {

}
