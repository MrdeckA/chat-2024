import { BadRequest } from "@feathersjs/errors";
import { generateRandomString } from "../../helpers/common-functions.js";


export const surveyCreateBefore = async (context) => {
  if (context.params?.user) {

    context.data.creatorId = context.params.user._id

    context.data.createdAt = Date.now()

    if (!context.data?.discussionId) {
      throw new BadRequest("You must provide discussionId");
    }

    var discussion = await context.app.service('discussions').get(context.data.discussionId, { query : {tag : 'GROUP'}});

    const finduser = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString() && participant.isAdmin);

    if (!finduser) {
      throw new BadRequest("You can not create survey this discussion");
    }

    if (!context.data.options || !Array.isArray(context.data.options)) {
      throw new BadRequest("You must provide array of options");
    }

    context.data.options =  [...new Set(context.data.options)]

    if (context.data.length < 2) {
      throw new BadRequest("You must provide at leats two options");
    }

    var options = []

    context.data.options.forEach(element => {
      options.push({
        id : generateRandomString(6),
        response: element,
        voters: []
      })
    });

    context.data.options = options

  }
  
}

export const surveyCreateAfter = async (context) => {
  context.app.service("messages").create({
    discussionId: context.result.discussionId,
    senderId: context.result.creatorId,
    text: context.result.question,
    createdAt: context.result.createdAt,
    surveyId: context.result._id
  });
}
