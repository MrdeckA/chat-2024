import { BadRequest } from "@feathersjs/errors";
import { uploadToLocal } from "../../helpers/uploadToLocal.js";

export const messageCreateBefore = async (context) => {
  if(context.params?.user) {
    context.data.senderId = context.params.user._id;

    if (context.data.contactId && !context.data.discussion) {
      const contact = await context.app.service('contacts').get(context.data.contactId, { query : {status : 'VALIDATED'}});
      
      if(![contact.user1Id.toString(), contact.user2Id.toString()].includes(context.params.user._id.toString())) {
         throw new BadRequest("This user is not your contact")
      }

      const senderIsUser1 = contact.user1Id.toString() === context.params.user._id.toString() ? true : false;

      if ((contact.user1blocked && senderIsUser1) || (contact.user2blocked && !senderIsUser1)) {
        throw new BadRequest("You have blocked your contact")
      }

      if ((contact.user1blocked && !senderIsUser1) || (contact.user2blocked && senderIsUser1)) {
        throw new BadRequest("You are blocked by your contact")
      }

      var discussion = await context.app.service('discussions').find({ query : {
        tag: 'PRIVATE',
        $and : [
          {members: {'$elemMatch': {'userId': contact.user1Id}}},
          {members: {'$elemMatch': {'userId': contact.user2Id}}},
        ],
        $limit: 1
      }});

      if (discussion.total < 1) {
        discussion = await context.app.service('discussions').create({
          tag : 'PRIVATE',
          createdBy: context.params.user._id,
          updatedAt : Date.now(),
          createdAt : Date.now(),
          members : [
            {
              userId: contact.user1Id,
              isAdmin : true,
              addedAt: Date.now()
            },
            {
              userId: contact.user2Id,
              isAdmin : true,
              addedAt: Date.now()
            },
          ]
        })

        context.data.discussionId = discussion._id
      } 
      else {
        context.data.discussionId = discussion.data[0]._id
      }

    }

    if (context.data.discussionId && !context.data.contactId) {
      const discussion = await context.app.service('discussions').get(context.data.discussionId);

      const finduser = discussion.members.find( member => member.userId.toString() == context.params.user._id.toString());

      if (!finduser) {
        throw new BadRequest("You can not send message to this discussion");
      }

      if (discussion.tag === 'PRIVATE') {
        
        const findReceiver = discussion.members.find( member => member.userId.toString() !== context.params.user._id.toString());
        const contact = await context.app.service('contacts').find({ query : {
          $or : [
            {user1Id : context.params.user._id, user2Id : findReceiver.userId},
            {user2Id : context.params.user._id, user1Id : findReceiver.userId},
          ],
          $limit: 1
        }});

        if ((contact.data[0].user1blocked && senderIsUser1) || (contact.data[0].user2blocked && !senderIsUser1)) {
          throw new BadRequest("You have blocked your contact")
        }
  
        if ((contact.data[0].user1blocked && !senderIsUser1) || (contact.data[0].user2blocked && senderIsUser1)) {
          throw new BadRequest("You are blocked by your contact")
        }

      }

    }

    delete context.data.contactId;
    delete context.data.surveyId
    context.data.createdAt = Date.now()
    context.data.senderId = context.params.user._id
    
  }

}

export const messageUploadFile = async (context) => {

  if (context.params.file) {
      var i = 0
      const { originalname, buffer, mimetype, size } = context.params.file
      let finalPath = await uploadToLocal(originalname, buffer, i.toString(), 'public/uploads')
      context.data.file = {
        name: originalname.normalize('NFD').replace(/\p{Diacritic}/gu, ''),
        type: mimetype,
        pathUrl: finalPath,
        size: size
      }
    }
  
    if (!context.data?.text && !context.data.file) {
      throw new BadRequest(
        'Message content can not be empty!'
      )
    }

}

export const messageCreateAfter = async (context) => {
  const {senderId, text, file, createdAt} = context.result;

  var discussion = await context.app.service('discussions').get(context.data.discussionId);

  for (var idx = 0; idx < discussion.members.length; idx++) {
    if (discussion.members[idx].userId.toString() !== senderId.toString()) {
      discussion.members[idx].hasNewNotif = true;
    }
  }

  discussion = await context.app.service('discussions').patch(context.result.discussionId, {
    lastMessage : {
      senderId, 
      text, 
      file, 
      createdAt
    },
    members : discussion.members
  });

  context.result.discussion = discussion

}
