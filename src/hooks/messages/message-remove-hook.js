export const messageRemoveBefore = async (context) => {

  if (context.params?.user) {

    context.params.query = {
      ...context.params.query,
       $or : [
         {user1Id : context.params.user._id},
         {user2Id : context.params.user._id}
       ] 
    }
  
  }

}

