import { BadRequest } from "@feathersjs/errors"
import { uploadToLocal } from "../../helpers/uploadToLocal.js";

export const discussionPatchBefore = async (context) => {


  if (context.params?.user) {

    var discussion = await context.app.service('discussions').get(context.id);

    if (!discussion) {
      throw new BadRequest("This discussion not exist");
    }

    const finduser = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString());

    if (!finduser) {
      throw new BadRequest("You can not patch this discussion");
    }

    const possibleActions = ['ARCHIVED', 'UPDATE_GROUP_INFO', 'REMOVE_USERS_GROUP', 'ADD_USERS_GROUP', 'DEFINE_ADMINS_GROUP', 'LEAVE_GROUP', 'OPEN_DISCUSSION', 'PINED', 'MUTED']

    if (!context.data.action || !possibleActions.includes(context.data.action)) {
      throw new BadRequest("Invalid action");
    }

    if (context.data.action === 'ARCHIVED') {
      const finduserIndex = discussion.members.findIndex( participant => participant.userId.toString() == context.params.user._id.toString());
      discussion.members[finduserIndex].isArchived = context.data.isArchived
      context.data.members = discussion.members.map((e) => { delete e.user; return e; });
      delete context.data?.isArchived;
    }

    else if (context.data.action === 'PINED') {
      const finduserIndex = discussion.members.findIndex( participant => participant.userId.toString() == context.params.user._id.toString());
      discussion.members[finduserIndex].isPined = context.data.isPined
      context.data.members = discussion.members.map((e) => { delete e.user; return e; });
      delete context.data?.isPined;
    }

    else if (context.data.action === 'MUTED') {
      const finduserIndex = discussion.members.findIndex( participant => participant.userId.toString() == context.params.user._id.toString());
      discussion.members[finduserIndex].isMuted = context.data.isMuted
      context.data.members = discussion.members.map((e) => { delete e.user; return e; });
      delete context.data?.isMuted;
    }

    else if (context.data.action === 'UPDATE_GROUP_INFO') {
      
      if (discussion.tag === 'PRIVATE') {
        throw new BadRequest("You can not update private discussion info");
      }

      const findUserAsAdmin = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString() && participant.isAdmin);
      if (!findUserAsAdmin) {
        throw new BadRequest("You can not perform this action");
      }
      const {name, description} = context.data;

      context.data = {name, description, updatedAt : Date.now()};
    }

    else if (context.data.action === 'LEAVE_GROUP') {
      
      if (discussion.tag === 'PRIVATE') {
        throw new BadRequest("You can not update private discussion info");
      }

      const findUser = discussion.members.find(member => member.userId.toString() === context.params.user._id.toString())

      if (!findUser) {
        throw new BadRequest("You don't belong to this discussion group")
      }

      let filtermembers = discussion.members.filter(participant => participant.userId.toString() !== context.params.user._id.toString());
      
      if (findUser.isAdmin) {
        let filterAdmins = filtermembers.filter(participant => participant.isAdmin);
       
        if (filterAdmins.length < 1) {
          const indexUserRandom = discussion.members.findIndex( member => member.userId.toString() ===  filtermembers[0].userId.toString() );
          discussion.members[indexUserRandom].isAdmin = true;
        }
      }

      context.data = {
        updatedAt: Date.now(),
        members: filtermembers.map((e) => { delete e.user; return e; })
    }

    }


    else if (context.data.action === 'OPEN_DISCUSSION') {

      for (var idx = 0; idx < discussion.members.length; idx++) {
        if (discussion.members[idx].userId.toString() === context.params.user._id.toString()) {
          discussion.members[idx].hasNewNotif = false;
          break;
        }
      }

      context.data = {
        updatedAt: Date.now(),
        members: discussion.members.map((e) => { delete e.user; return e; })
      }
    }


    else if (context.data.action === 'REMOVE_USERS_GROUP') {

      const findUserAsAdmin = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString() && participant.isAdmin);
      
      if (!findUserAsAdmin) {
        throw new BadRequest("You can not perform this action");
      }

      if (!Array.isArray(context.data.removeUsers) || context.data.removeUsers.length < 1) {
        throw new BadRequest("You must give at least one user to remove")
      }

      let filtermembers = discussion.members.filter(participant => !context.data.removeUsers.includes(participant.userId.toString()));

      if (filtermembers.length < 3) {
        throw new BadRequest("You have to be at least 3 members in discussion group")
      }

      context.data = {
        updatedAt: Date.now(),
        members: filtermembers.map((e) => { delete e.user; return e; })
      }
    }

    else if (context.data.action === 'ADD_USERS_GROUP') {
      
      if (discussion.tag === 'PRIVATE') {
        throw new BadRequest("You can not update private discussion info");
      }

      const findUserAsAdmin = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString() && participant.isAdmin);
      
      if (!findUserAsAdmin) {
        throw new BadRequest("You can not perform this action");
      }

      if (!Array.isArray(context.data.addUsers) || context.data.addUsers.length < 1) {
        throw new BadRequest("You must give at least one user to add")
      }

      context.data.addUsers.forEach(user => {
        const findExistParticipant = discussion.members.find(paticipant => paticipant.userId.toString() === user.toString());
        if (!findExistParticipant) {
          discussion.members.push({
            userId: user,
            isAdmin: false,
            isPined: false,
            isMuted: false,
            hasNewNotif: true,
            isArchived: false,
            addedAt : Date.now(),
          })
        }
      });

      context.data = {
        updatedAt: Date.now(),
        members: discussion.members.map((e) => { delete e.user; return e; })
      }

    }

    else if (context.data.action === 'DEFINE_ADMINS_GROUP') {
      
      if (discussion.tag === 'PRIVATE') {
        throw new BadRequest("You can not update private discussion info");
      }

      const findUserAsAdmin = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString() && participant.isAdmin);
      
      if (!findUserAsAdmin) {
        throw new BadRequest("You can not perform this action");
      }

      if (!Array.isArray(context.data.adminUsers) || context.data.adminUsers.length < 1) {
        throw new BadRequest("You must give at least one user to define as admin")
      }

      for (var idx = 0; idx < discussion.members.length; idx++) {
        if (context.data.adminUsers.includes(discussion.members[idx].userId.toString())) {
          discussion.members[idx].isAdmin = true;
        }
      }

      context.data = {
        updatedAt: Date.now(),
        members: discussion.members.map((e) => { delete e.user; return e; })
      }
    }

    else {
      context.data = {};
    }

    delete context.data?.action;

  }

}

export const discussionUploadFile = async (context) => {

  if (context.params.file) {
      var i = 0
      const { originalname, buffer, mimetype, size } = context.params.file
      let finalPath = await uploadToLocal(originalname, buffer, i.toString(), 'public/discussions')
      context.data.photoUrl = finalPath
    }
}

export const discussionPatchAfter = async (context) => {

}
