import { BadRequest } from "@feathersjs/errors";

export const discussionRemoveBefore = async (context) => {

  if (context.params?.user) {

    var discussion = await context.app.service('discussions').get(context.id);

    if (!discussion) {
      throw new BadRequest("This discussion not exist");
    }

    const finduser = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString() && participant.isAdmin);

    if (!finduser) {
      throw new BadRequest("You can not delete this discussion");
    }
  }
}


export const discussionRemoveAfter = async (context) => {

  context.app.service('messages').remove(null, { query : { discussionId: context.result._id}});
  
}

