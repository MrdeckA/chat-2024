import { BadRequest } from "@feathersjs/errors";


export const discussionCreateBefore = async (context) => {
  if (context.params?.user) {
    context.data.createdBy = context.params.user._id
  }

  if (!context.data?.tag || !['GROUP', 'PRIVATE'].includes(context.data.tag)) {
    throw new BadRequest("Your action is not valid")
  }

  if (context.data.tag === 'GROUP') {
    
    if (!context.data.members || !Array.isArray(context.data.members)) {
        throw new BadRequest("You have to provide an array of members")
    }

    context.data.members.push({
      userId : context.params.user._id.toString(),
      isAdmin: true
    })

        //TODO : Filter to unique value, check this line again
    //context.data.members = [...new Set(context.data.members.map(item => item.group))];

    if (context.data.members < 3) {
      throw new BadRequest("You have to be at least 3 in group discussion")
    }

    const nowDate = Date.now();

    context.data.updatedAt = nowDate;
    context.data.createdAt = nowDate;

    let members = []

    context.data.members.forEach(element => {
      members.push({
        userId: element.userId,
        isAdmin: element.isAdmin,
        addedAt : nowDate
      })
    });

    context.data.members = members;
  
  }

  
}
