// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve } from '@feathersjs/schema'
import { Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { ObjectIdSchema } from '@feathersjs/typebox'
import { passwordHash } from '@feathersjs/authentication-local'
import { dataPatchValidator, dataValidator, queryValidator } from '../../validators.js'
import { BadRequest } from '@feathersjs/errors'

// Main data model schema
export const userSchema = Type.Object(
  {
    _id: ObjectIdSchema(),
    email: Type.String(),
    password: Type.String({minLength: 6}),
    photoUrl: Type.Union([Type.Null(), Type.String()], {default: null}),
    lastname: Type.String(),
    firstname: Type.String(),
    status: Type.Union([Type.Null(), Type.String()], {default: null}),
  },
  { $id: 'User', additionalProperties: false }
)
export const userValidator = getValidator(userSchema, dataValidator)

export const userResolver = resolve({})

export const userExternalResolver = resolve({
  // The password should never be visible externally
  password: async () => undefined
})

// Schema for creating new entries
export const userDataSchema = Type.Omit(userSchema, ['_id'], {
  $id: 'UserData'
})
export const userDataValidator = getValidator(userDataSchema, dataValidator)
export const userDataResolver = resolve({
  password: (password, user, context) => {
    let passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (!password.match(passwordRegex)) {
      throw new BadRequest("Password submited must be 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character");
    }
    return passwordHash({ strategy: 'local' })(password, user, context)
  },
  email : async (email, user, context) => {
    let exitUser = await context.app.service('users').find({query : {email: email, $limit: 1}})
    if (exitUser.total > 0) {
      throw new BadRequest("This email is already used");
    }
    return email;
  }
})

// Schema for updating existing entries
export const userPatchSchema = Type.Partial(userSchema, {
  $id: 'UserPatch'
})
export const userPatchValidator = getValidator(userPatchSchema, dataPatchValidator)
export const userPatchResolver = resolve({
  email : async (email, user, context) => {
    if(email) {
      let exitUser = await context.app.service('users').find({query : {email: email, $limit: 1}})
      if (exitUser.total > 0 && exitUser.data[0]._id.toString() !== context.params.user._id.toString()) {
        throw new BadRequest("This email is already used");ß
      }
    }
    return email;
  },
  password: passwordHash({ strategy: 'local' })
})

// Schema for allowed query properties
export const userQueryProperties = Type.Omit(userSchema, [])
export const userQuerySchema = Type.Intersect(
  [
    querySyntax(userQueryProperties,  {
      lastname: {
        $regex: Type.String({minLength: 3}),
        $options : Type.String()
      },
      firstname: {
        $regex: Type.String({minLength: 3}),
        $options : Type.String()
      },
      email: {
        $regex: Type.String({minLength: 3}),
        $options : Type.String()
      },
    }),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const userQueryValidator = getValidator(userQuerySchema, queryValidator)
export const userQueryResolver = resolve({

})
