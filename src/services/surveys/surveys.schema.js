// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve } from '@feathersjs/schema'
import { Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { ObjectIdSchema } from '@feathersjs/typebox'
import { dataPatchValidator, dataValidator, queryValidator } from '../../validators.js'
import { discussion } from '../discussions/discussions.js'
import { BadRequest } from '@feathersjs/errors'

// Main data model schema
export const surveySchema = Type.Object(
  {
    _id: ObjectIdSchema(),
    question: Type.String(),
    discussionId: ObjectIdSchema(),
    creatorId: ObjectIdSchema(),
    options: Type.Array(
      Type.Object({
        id: Type.String(),
        response: Type.String(),
        voters: Type.Array(ObjectIdSchema(), {minItems: 0, default: []})
      }),
      {minItems: 2, maxItems: 5}
    ),
    createdAt: Type.Number(),
  },
  { $id: 'Survey', additionalProperties: false }
)
export const surveyValidator = getValidator(surveySchema, dataValidator)
export const surveyResolver = resolve({
  options : async (value, discussion, context) => {
    if(value) {
      for (var idx = 0; idx < value.length; idx++) {
        var voters = []
        for (var idx1 = 0; idx1 < value[idx].voters.length; idx1++) {
          const user = await context.app.service('users').get(value[idx].voters[idx1], { query : {$select : ['_id', 'firstname', 'lastname', 'email', 'photoUrl']} });
          voters.push(user);
        }
        value[idx].voters = voters
      }
    }
    return value;
  }
})

export const surveyExternalResolver = resolve({})

// Schema for creating new entries
export const surveyDataSchema = Type.Omit(surveySchema, ['_id'], {
  $id: 'SurveyData'
})
export const surveyDataValidator = getValidator(surveyDataSchema, dataValidator)
export const surveyDataResolver = resolve({
  discussionId : async (value, survey, context) => {
    if(value) {
      const exitDiscussion = await context.app.service('discussions').get(value);
      if(!exitDiscussion) {
        throw new BadRequest("discussion "+ value +" not exist");
      }
    }
    return value;
  },
  creatorId : async (value, survey, context) => {
    if(value) {
      const existUser = await context.app.service('users').get(value);
      if(!existUser) {
        throw new BadRequest("user "+ value +" not exist");
      }
    }
    return value;
  }
})

// Schema for updating existing entries
export const surveyPatchSchema = Type.Partial(surveySchema, {
  $id: 'SurveyPatch'
})
export const surveyPatchValidator = getValidator(surveyPatchSchema, dataPatchValidator)
export const surveyPatchResolver = resolve({})

// Schema for allowed query properties
export const surveyQueryProperties = Type.Omit(surveySchema, [])
export const surveyQuerySchema = Type.Intersect(
  [
    querySyntax(surveyQueryProperties),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const surveyQueryValidator = getValidator(surveyQuerySchema, queryValidator)
export const surveyQueryResolver = resolve({})
