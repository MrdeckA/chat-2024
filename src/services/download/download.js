// For more information about this file see https://dove.feathersjs.com/guides/cli/service.html
import { authenticate } from '@feathersjs/authentication'

import { DownloadService, getOptions } from './download.class.js'
import { BadRequest } from '@feathersjs/errors'
import { downloadFileMiddleware } from '../../middlewares/download-files-middleware.js'

export const downloadPath = 'download'
export const downloadMethods = ['find']

export * from './download.class.js'

// A configure function that registers the service and its hooks via `app.configure`
export const download = (app) => {
  // Register our service on the Feathers application
  app.use(downloadPath, new DownloadService(getOptions(app)), {
    // A list of all methods this service exposes externally
    methods: downloadMethods,
    // You can add additional custom events to be sent to clients here
    events: [],
    koa: {
      after: [downloadFileMiddleware]
    }
  })
  // Initialize hooks
  app.service(downloadPath).hooks({
    around: {
      all: [
        authenticate('jwt'),
      ]
    },
    before: {
  
      find: [
          async (context, next) => {
            const { messageId } = context.params.query
  
            if (!messageId) {
              throw new BadRequest("You have to provide message id")
            }
  
            const message = await context.app.service('messages').get(messageId);
  
            if (!message.file?.name) {
              throw new BadRequest("Any file is not associate to this message")
            }
  
            if (context.params?.user) {
              if (message.discussionId) {
                const discussion = await context.app.service('discussions').get(message.discussionId);
                const findUser = discussion.members.find(
                  (member) => member.userId.toString() == context.params.user._id.toString()
                )
                if (!findUser) {
                  throw new BadRequest("You can not access to this file")
                }
              }
  
            }
  
            context.params.file = message.file
          }
  
        ],
      
    },
    after: {
      all: []
    },
    error: {
      all: []
    }
  })
}
