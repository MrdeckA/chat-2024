// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve, virtual } from '@feathersjs/schema'
import { StringEnum, Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { ObjectIdSchema } from '@feathersjs/typebox'
import { dataPatchValidator, dataValidator, queryValidator } from '../../validators.js'
import { BadRequest } from '@feathersjs/errors'

// Main data model schema
export const contactSchema = Type.Object(
  {
    _id: ObjectIdSchema(),
    user1Id: ObjectIdSchema(),
    user2Id: ObjectIdSchema(),
    user1Blocked: Type.Boolean({default: false}),
    user2Blocked: Type.Boolean({default: false}),
    addedAt: Type.Number(),
    status: StringEnum(['PENDING', 'VALIDATED', 'DECLINED'], {default: 'PENDING'}),
  },
  { $id: 'Contact', additionalProperties: false }
)
export const contactValidator = getValidator(contactSchema, dataValidator)
export const contactResolver = resolve({
  user1: virtual(async (contact, context) => {
    return await context.app.service('users').get(contact.user1Id)
  }),
  user2: virtual(async (contact, context) => {
    return await context.app.service('users').get(contact.user2Id)
  }),
})

export const contactExternalResolver = resolve({})

// Schema for creating new entries
export const contactDataSchema = Type.Omit(contactSchema, ['_id'], {
  $id: 'ContactData'
})
export const contactDataValidator = getValidator(contactDataSchema, dataValidator)
export const contactDataResolver = resolve({
  user2Id: async (user2Id, contact, context) => {

    if (user2Id.toString() === contact.user1Id.toString()) {
      throw new BadRequest("You can not send contact request to yourself")
    }

    const user2 = await context.app.service('users').find({query : {_id : user2Id, $limit: 1}});
    if (!user2 || user2.total < 1) {
      throw new BadRequest("User 2 not existed")
    }

    const existContact = await context.app.service('contacts').find({
      query : {
        $or : [
          {
            user1Id: contact.user1Id,
            user2Id : user2Id
          },
          {
            user1Id : user2Id,
            user2Id: contact.user1Id
          }
        ],
        $limit: 1
      }
    })

    if (existContact && existContact.total > 0) {
      throw new BadRequest("you are already connect to this user")
    }

    return user2Id
  }
})

// Schema for updating existing entries
export const contactPatchSchema = Type.Partial(contactSchema, {
  $id: 'ContactPatch'
})
export const contactPatchValidator = getValidator(contactPatchSchema, dataPatchValidator)
export const contactPatchResolver = resolve({})

// Schema for allowed query properties
export const contactQueryProperties = Type.Omit(contactSchema, [])
export const contactQuerySchema = Type.Intersect(
  [
    querySyntax(contactQueryProperties),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const contactQueryValidator = getValidator(contactQuerySchema, queryValidator)
export const contactQueryResolver = resolve({})
