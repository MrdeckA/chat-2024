// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve, virtual} from '@feathersjs/schema'
import { StringEnum, Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { ObjectIdSchema } from '@feathersjs/typebox'
import { dataPatchValidator, dataValidator, queryValidator } from '../../validators.js'
import { BadRequest } from '@feathersjs/errors'

// Main data model schema
export const discussionSchema = Type.Object(
  {
    _id: ObjectIdSchema(),
    name: Type.Union([Type.Null(), Type.String()], {default: null}),
    description: Type.Union([Type.Null(), Type.String()], {default: null}),
    tag: StringEnum(['GROUP', 'PRIVATE']),
    createdBy: ObjectIdSchema(),
    photoUrl: Type.Union([Type.Null(), Type.String()], {default: null}),
    createdAt: Type.Number(),
    updatedAt: Type.Number(),
    members: Type.Array(Type.Object({
      userId: ObjectIdSchema(),
      isPined: Type.Boolean({default: false}),
      hasNewNotif: Type.Boolean({default: true}),
      isMuted: Type.Boolean({default: false}),
      isAdmin: Type.Boolean({default: false}),
      isArchived: Type.Boolean({default: false}),
      addedAt: Type.Number()
    })),
    lastMessage: Type.Union([Type.Null(), Type.Any()], {default: null})
  },
  { $id: 'Discussion', additionalProperties: false }
)
export const discussionValidator = getValidator(discussionSchema, dataValidator)
export const discussionResolver = resolve({
  createdByUser : virtual(async (discussion, context) => await context.app.service("users").get(discussion.createdBy)),
  members : async (value, discussion, context) => {
    if(value) {
      for (var idx = 0; idx < value.length; idx++) {
        const user = await context.app.service('users').get(value[idx].userId, { query : {$select : ['_id', 'firstname', 'lastname', 'email', 'photoUrl']} });
        value[idx].user = user;
      }
    }
    return value;
  }
})

export const discussionExternalResolver = resolve({})

// Schema for creating new entries
export const discussionDataSchema = Type.Omit(discussionSchema, ['_id'], {
  $id: 'DiscussionData'
})
export const discussionDataValidator = getValidator(discussionDataSchema, dataValidator)
export const discussionDataResolver = resolve({

  name : (value, discussion, context) => {
    if (!value && discussion.tag === 'GROUP') {
      throw new BadRequest("Group name is required")
    }
    return value
  },

  description : (value, discussion, context) => {
    if (!value && discussion.tag === 'GROUP') {
      throw new BadRequest("Group description is required")
    }
    return value
  },

  members: async (members, discussion, context) => {
    for (const member of members) {
      const user = await context.app.service('users').find({query : {_id : member.userId, $limit: 1}});
      if (!user || user.total < 1) {
        throw new BadRequest("User "+ member.userId +" not existed")
      }
    }
    return members
  },

})

// Schema for updating existing entries
export const discussionPatchSchema = Type.Partial(discussionSchema, {
  $id: 'DiscussionPatch'
})
export const discussionPatchValidator = getValidator(discussionPatchSchema, dataPatchValidator)
export const discussionPatchResolver = resolve({
  members : async (value, discussion, context) => {
    if(value) {
      for (let participant of value) {
        const exitUser = await context.app.service('users').get(participant.userId);
        if(!exitUser) {
          throw new BadRequest("user "+ participant.userId +" not exist");
        }
      }
    }
    return value;
  }

})

// Schema for allowed query properties
export const discussionQueryProperties = Type.Omit(discussionSchema, [])
export const discussionQuerySchema = Type.Intersect(
  [
    querySyntax(discussionQueryProperties, {
      members: {
        $elemMatch: Type.Object({
          userId: Type.Optional(ObjectIdSchema()),
          isAdmin: Type.Optional(Type.Boolean()),
          hasNewNotif: Type.Optional(Type.Boolean()),
          isArchived: Type.Optional(Type.Boolean()),
          isPined: Type.Optional(Type.Boolean()),
        }),
      }
    }),
    // Add additional query properties here
    Type.Object({
      'members.userId' : Type.Optional(ObjectIdSchema())
    }, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const discussionQueryValidator = getValidator(discussionQuerySchema, queryValidator)
export const discussionQueryResolver = resolve({})
