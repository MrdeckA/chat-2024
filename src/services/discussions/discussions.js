// For more information about this file see https://dove.feathersjs.com/guides/cli/service.html
import { authenticate } from '@feathersjs/authentication'

import { hooks as schemaHooks } from '@feathersjs/schema'
import {
  discussionDataValidator,
  discussionPatchValidator,
  discussionQueryValidator,
  discussionResolver,
  discussionExternalResolver,
  discussionDataResolver,
  discussionPatchResolver,
  discussionQueryResolver
} from './discussions.schema.js'
import { DiscussionService, getOptions } from './discussions.class.js'
import { discussionCreateBefore } from '../../hooks/discussions/discussion-create-hook.js'
import { discussionPatchBefore, discussionUploadFile } from '../../hooks/discussions/discussion-patch-hook.js'
import { discussionRemoveBefore } from '../../hooks/discussions/discussion-remove-hook.js'
import { discussionGetBefore } from '../../hooks/discussions/discussion-get-hook.js'
import { discussionFindBefore } from '../../hooks/discussions/discussion-find-hook.js'
import { multipartMiddlewareImage, uploadFileMiddleware } from '../../middlewares/upload-files-middleware.js'

export const discussionPath = 'discussions'
export const discussionMethods = ['find', 'get', 'create', 'patch', 'remove']

export * from './discussions.class.js'
export * from './discussions.schema.js'

// A configure function that registers the service and its hooks via `app.configure`
export const discussion = (app) => {
  // Register our service on the Feathers application
  app.use(discussionPath, new DiscussionService(getOptions(app)), {
    // A list of all methods this service exposes externally
    methods: discussionMethods,
    // You can add additional custom events to be sent to clients here
    events: [],
    koa: {
      before: [multipartMiddlewareImage.single('file'), uploadFileMiddleware]
    }
  })
  // Initialize hooks
  app.service(discussionPath).hooks({
    around: {
      all: [
        authenticate('jwt'),
        schemaHooks.resolveExternal(discussionExternalResolver),
        schemaHooks.resolveResult(discussionResolver)
      ]
    },
    before: {
      all: [
        schemaHooks.validateQuery(discussionQueryValidator),
        schemaHooks.resolveQuery(discussionQueryResolver)
      ],
      find: [
        discussionFindBefore
      ],
      get: [
        discussionGetBefore
      ],
      create: [
        discussionCreateBefore,
        schemaHooks.validateData(discussionDataValidator),
        schemaHooks.resolveData(discussionDataResolver)
      ],
      patch: [
        discussionPatchBefore,
        schemaHooks.validateData(discussionPatchValidator),
        schemaHooks.resolveData(discussionPatchResolver),
        discussionUploadFile
      ],
      remove: [
        discussionRemoveBefore
      ]
    },
    after: {
      all: []
    },
    error: {
      all: []
    }
  })
}
