// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve, virtual } from '@feathersjs/schema'
import { Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { ObjectIdSchema } from '@feathersjs/typebox'
import { dataPatchValidator, dataValidator, queryValidator } from '../../validators.js'
import { BadRequest } from '@feathersjs/errors'

// Main data model schema
export const messageSchema = Type.Object(
  {
    _id: ObjectIdSchema(),
    text: Type.Union([Type.Null(), Type.String()], {default: null}),
    senderId: ObjectIdSchema(),
    surveyId: Type.Union([Type.Null(), ObjectIdSchema()], {default: null}),
    discussionId: ObjectIdSchema(),
    responseToMsgId: Type.Union([Type.Null(), ObjectIdSchema()], {default: null}),
    file: Type.Union([Type.Null(), Type.Object({
      name: Type.String(),
      pathUrl: Type.String(),
      size: Type.Number(),
      type: Type.String(),
    })], {default : null}), 
    reactions: Type.Array(Type.Object({
      userId: ObjectIdSchema(),
      emoji: Type.String()
    }), {default: []}),
    createdAt: Type.Number(),
  },
  { $id: 'Message', additionalProperties: false }
)
export const messageValidator = getValidator(messageSchema, dataValidator)
export const messageResolver = resolve({
  sender: virtual(async (message, context) => {
    return await context.app.service('users').get(message.senderId)
  }),
  survey: virtual(async (message, context) => {
    return message.surveyId ? await context.app.service('surveys').get(message.surveyId) : undefined
  }),
  responseToMessage: virtual(async (message, context) => {
    return message.responseToMsgId ? await context.app.service('messages').get(message.responseToMsgId, {query : {$select: ['_id', 'text', 'file', 'senderId', 'sender', 'createdAt']}}) : undefined;
  }),
})

export const messageExternalResolver = resolve({})

// Schema for creating new entries
export const messageDataSchema = Type.Omit(messageSchema, ['_id'], {
  $id: 'MessageData'
})
export const messageDataValidator = getValidator(messageDataSchema, dataValidator)
export const messageDataResolver = resolve({

  reactions : async (value, message, context) => {
    if(value && value.length > 0) {
      for (let reaction of value) {
        const exitUser = await context.app.service('users').get(reaction.userId);
        if(!exitUser) {
          throw new BadRequest("user "+ reaction.userId +" not exist");
        }
      }
    }
    return value;
  },
  responseToMessageId : async (value, message, context) => {
    if(value) {
      const exitMessage = await context.app.service('messages').get(value);
      if(!exitMessage) {
        throw new BadRequest("message "+ value +" not exist");
      }
    }
    return value;
  },
  discussionId : async (value, message, context) => {
    if(value) {
      const exitDiscussion = await context.app.service('discussions').get(value);
      if(!exitDiscussion) {
        throw new BadRequest("discussion "+ value +" not exist");
      }
    }
    return value;
  }

})

// Schema for updating existing entries
export const messagePatchSchema = Type.Partial(messageSchema, {
  $id: 'MessagePatch'
})
export const messagePatchValidator = getValidator(messagePatchSchema, dataPatchValidator)
export const messagePatchResolver = resolve({
  reactions : async (value, message, context) => {
    if(value && value.length > 0) {
      for (let reaction of value) {
        const exitUser = await context.app.service('users').get(reaction.userId);
        if(!exitUser) {
          throw new BadRequest("user "+ reaction.userId +" not exist");
        }
      }
    }
    return value;
  },
  responseToMessageId : async (value, message, context) => {
    if(value) {
      const exitMessage = await context.app.service('messages').get(value);
      if(!exitMessage) {
        throw new BadRequest("message "+ value +" not exist");
      }
    }
    return value;
  },
  discussionId : async (value, message, context) => {
    if(value) {
      const exitDiscussion = await context.app.service('discussions').get(value);
      if(!exitDiscussion) {
        throw new BadRequest("discussion "+ value +" not exist");
      }
    }
    return value;
  }
})

// Schema for allowed query properties
export const messageQueryProperties = Type.Omit(messageSchema, [])
export const messageQuerySchema = Type.Intersect(
  [
    querySyntax(messageQueryProperties),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const messageQueryValidator = getValidator(messageQuerySchema, queryValidator)
export const messageQueryResolver = resolve({})
