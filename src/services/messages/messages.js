// For more information about this file see https://dove.feathersjs.com/guides/cli/service.html
import { authenticate } from '@feathersjs/authentication'

import { uploadFileMiddleware, multipartMiddlewareMessage } from '../../middlewares/upload-files-middleware.js'

import { hooks as schemaHooks } from '@feathersjs/schema'
import {
  messageDataValidator,
  messagePatchValidator,
  messageQueryValidator,
  messageResolver,
  messageExternalResolver,
  messageDataResolver,
  messagePatchResolver,
  messageQueryResolver
} from './messages.schema.js'
import { MessageService, getOptions } from './messages.class.js'
import { messageCreateAfter, messageCreateBefore, messageUploadFile } from '../../hooks/messages/message-create-hook.js'
import { messagePatchBefore } from '../../hooks/messages/message-patch-hook.js'
import { messageFindBefore } from '../../hooks/messages/message-find-hook.js'

export const messagePath = 'messages'
export const messageMethods = ['find', 'get', 'create', 'patch', 'remove']

export * from './messages.class.js'
export * from './messages.schema.js'

// A configure function that registers the service and its hooks via `app.configure`
export const message = (app) => {
  // Register our service on the Feathers application
  app.use(messagePath, new MessageService(getOptions(app)), {
    // A list of all methods this service exposes externally
    methods: messageMethods,
    // You can add additional custom events to be sent to clients here
    events: [],
    koa: {
      before: [multipartMiddlewareMessage.single('file'), uploadFileMiddleware]
    }
  })
  // Initialize hooks
  app.service(messagePath).hooks({
    around: {
      all: [
        authenticate('jwt'),
        schemaHooks.resolveExternal(messageExternalResolver),
        schemaHooks.resolveResult(messageResolver)
      ]
    },
    before: {
      all: [schemaHooks.validateQuery(messageQueryValidator), schemaHooks.resolveQuery(messageQueryResolver)],
      find: [messageFindBefore],
      get: [],
      create: [messageCreateBefore, schemaHooks.validateData(messageDataValidator), schemaHooks.resolveData(messageDataResolver), messageUploadFile],
      patch: [messagePatchBefore, schemaHooks.validateData(messagePatchValidator), schemaHooks.resolveData(messagePatchResolver)],
      remove: []
    },
    after: {
      all: [],
      create: [messageCreateAfter]
    },
    error: {
      all: []
    }
  })
}
